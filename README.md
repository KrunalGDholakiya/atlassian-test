# Atlassian UI Coding Test #

Step: 1 - Start mock APIs server

  a. Goto 'api' folder.
  b. Run 'npm install' to install all node modules.
  c. Run 'npm start' to start server.

Step: 2 - Start webserver

  a. Goto  'frontend' folder.
  b. Run 'npm install' to install all node modules.
  c. Run 'nom start' to start web server.
  Note: Application will be accessed at http://localhost:3000.
  Note: To run web application against different server, change host inside constants.js file:
     |
     |--->frontend
              |
              |--->app
                    |
                    |--->constants.js
