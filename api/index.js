var mockService = require('osprey-mock-service')
var express = require('express')
var parser = require('raml-1-parser')
var path = require('path')
var osprey = require('osprey')

var app = express()
var cors = require('cors')
app.use(cors())

parser.loadRAML(path.join(__dirname, '/assets/space-api.raml'), { rejectOnErrors: true })
  .then(function (ramlApi) {
    var raml = ramlApi.expand(true).toJSON({ serializeMetadata: false })
    app.use(osprey.server(raml))
    app.use(mockService(raml))
    app.listen(4000)
    console.log("Server started on port 4000");
  })
