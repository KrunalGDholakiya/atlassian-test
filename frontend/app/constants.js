const host = "http://0.0.0.0:4000";

const constants = {
  'spaces': host+"/space",
  'space': host+"/space/",
  'users': host+"/users/",
  'entries': "/entries",
  'assets': "/assets"
};

export default constants;
