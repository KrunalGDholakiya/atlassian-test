import PropTypes from 'prop-types';
import React from 'react';
import {Router, Route} from 'react-router-dom';
import Routes from '../routes';

export default function Root({history}) {
    return (
            <div>
              <Router history={history}>
                  {Routes}
              </Router>
            </div>
    );
}
