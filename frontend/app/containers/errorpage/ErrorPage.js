import React from 'react'
import '../../assets/styles/errorpage.scss'
export default class ErrorPage extends React.Component {

  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div className="error_page">
        <img className="error_img" src={require('../../assets/images/error.png')} />
        <span className="error_message">{this.props.history.location.state.message}!</span>
      </div>
    )
  }
}
