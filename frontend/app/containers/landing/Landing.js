import React from 'react'
import '../../assets/styles/landing.scss'
import {Link} from 'react-router-dom'
import constants from '../../constants'
import {Container,Row,Col} from 'reactstrap'
export default class Landing extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      spaces: []
    }
  }

  componentDidMount() {
    // Getting list of spaces from server
    fetch(constants.spaces)
      .then(response => response.json())
      .then(function(data){
        // Redirecting to space explorer page
        this.props.history.push({pathname: '/spaceexplorer/'+data.items[0].sys.id, state:data.items})
      }.bind(this))
      .catch(function(error){
        // Redirecting to error page in failure
        this.props.history.push({pathname: '/error', state:{message: error.message}})
      }.bind(this));
  }

  render() {
    return (
      <div className="loader"></div>
    )
  }
}
