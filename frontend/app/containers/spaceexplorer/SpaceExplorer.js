import React from 'react'
import '../../assets/styles/spaceexplorer.scss'
import {Link} from 'react-router-dom'
import constants from '../../constants'
import {Container,Row,Col} from "reactstrap"
import Table from '../table/Table'

export default class SpaceExplorer extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      spaces:{},
      space:null,
      space_title:'',
      space_description:'',
      created_by:'',
      users:{},
    }
  }

  componentWillMount(){
    //building data to populate side menu
    let spaces = {}
    this.props.location.state.forEach(function(space){
      spaces[space.sys.id] = space
    })
    this.setState({spaces:spaces})
    //fetching list of users to display name in table and for created by
    fetch(constants.users)
      .then(response => response.json())
      .then(function(data){
        let users = {}
        data.items.forEach(function(user){
          users[user.sys.id] = user.fields.name
        })
        this.setState({users:users})
        // populating data on page
        this.fetchSpaceData(this.props.match.params.id);
      }.bind(this))
      .catch(function(error){
        this.props.history.push({pathname: '/error', state:{message: error.message}})
      }.bind(this));
  }

  fetchSpaceData(id){
    this.props.history.push({pathname: '/spaceexplorer/'+id, state:this.props.location.state})
    this.setState({
      space:this.state.spaces[id],
      space_title:this.state.spaces[id].fields.title,
      space_description:this.state.spaces[id].fields.description,
      created_by:this.state.users[this.state.spaces[id].sys.createdBy]
    })
  }
  
  render() {
    return (
      <div className="space-main">
        <Row className="space-container">
          <Col md="2" className="side-bar">
            <div>
              {
                this.props.location.state.map(function(space){
                  return(
                    <div className="spaces" key={space.sys.id} onClick={()=>this.fetchSpaceData(space.sys.id)}>
                      {space.fields.title}
                    </div>
                  )
                }, this)
              }
            </div>
          </Col>
          <Col md="10">
            <div className="header">
              <span className="space-name">{this.state.space_title}</span><span className="created-by">created by</span><span className="user-name">{this.state.created_by}</span>
            </div>
            <div className="description-container">
              <div className="description"><span className="description-text">{this.state.space_description}</span></div>
            </div>
            <div className="grid-container">
              <Table location={this.props.location} space_id={this.props.match.params.id} users={this.state.users}/>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}
