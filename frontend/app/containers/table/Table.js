import React from 'react'
import '../../assets/styles/table.scss'
import { Container, Row, Col, TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap'
import classnames from 'classnames'
import constants from '../../constants';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

export default class Table extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      activeTab: '1',
      entries:[],
      assets:[]
    };
    this.entries_columns = [
      {
        field: 'title',
        headerName: 'Title',
      },
      {
        field: 'summary',
        headerName: 'Summary',
      },
      {
        field: 'created_by',
        headerName: 'Created By'
      },
      {
        field: 'updated_by',
        headerName: 'Updated By'
      },
      {
        field: 'last_updated',
        headerName: 'Last Updated'
      },
    ]
    this.assets_columns = [
      {
        field: 'title',
        headerName: 'Title'
      },
      {
        field: 'content_type',
        headerName: 'Content Type'
      },
      {
        field: 'file_name',
        headerName: 'File Name'
      },
      {
        field: 'created_by',
        headerName: 'Created By'
      },
      {
        field: 'updated_by',
        headerName: 'Updated By'
      },
      {
        field: 'last_updated',
        headerName: 'Last Updated'
      },
    ]
  }
  componentWillMount(){
    // fetch table data
    this.getTableData()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      // check for different space and populate table
      this.getTableData()
      this.toggle('1')
    }
  }

  getTableData(){
    if(this.props.space_id === 'yadj1kx9rmg0'){
      // fetching data for entries table
      fetch(constants.space+this.props.space_id+constants.entries)
      .then(response => response.json())
      .then(function(entries){
        let row = [];
        for (var value of entries.items) {
          var date = new Date(value.sys.updatedAt);
          row.push({
            title: value.fields.title,
            summary: value.fields.summary,
            created_by: this.props.users[value.sys.createdBy],
            last_updated: date.getUTCMonth()+"-"+date.getUTCDate()+"-"+date.getUTCFullYear(),
            updated_by: this.props.users[value.sys.updatedBy]
          })
        }
        this.setState({
          entries:row
        })
      }.bind(this))
      .catch(function(error){
        this.props.history.push({pathname: '/error', state:{message: error.message}})
      }.bind(this));

      fetch(constants.space+this.props.space_id+constants.assets)
      .then(response => response.json())
      .then(function(assets){
        let row = [];
        for (var value of assets.items) {
          var date = new Date(value.sys.updatedAt);
          row.push({
            title: value.fields.title,
            content_type: value.fields.contentType,
            file_name: value.fields.fileName,
            created_by: this.props.users[value.sys.createdBy],
            last_updated: date.getUTCMonth()+"-"+date.getUTCDate()+"-"+date.getUTCFullYear(),
            updated_by: this.props.users[value.sys.updatedBy]
          })
        }
        this.setState({
          assets:row,
        })
        }.bind(this))
        .catch(function(error){
          this.props.history.push({pathname: '/error', state:{message: error.message}})
        }.bind(this));
      }
      else{
        this.setState({
          entries:[],
          assets:[]
        });
      }
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
    if(tab === "1"){
      this.setState({
        originalRows: this.state.originalEntries,
        rows: this.state.entries.slice(0)
      });
    }else if(tab === "2"){
      this.setState({
        originalRows: this.state.originalAssets,
        rows: this.state.assets.slice(0)
      });
    }
    this.getTableData()
  }

  renderEntriesTable(){
    if(this.state.entries.length > 0){
      return(
        <div className="ag-theme-balham table">
          <AgGridReact
            enableSorting={true}
            columnDefs={this.entries_columns}
            rowData={this.state.entries} />
        </div>
      )
    }else{
      return(
        <div>
          <span>Not Available</span>
        </div>
      )
    }
  }
  renderAssetsTable(){
    if(this.state.assets.length > 0){
      return(
        <div className="ag-theme-balham table">
          <AgGridReact
            enableSorting={true}
            columnDefs={this.assets_columns}
            rowData={this.state.assets} />
        </div>
      )
    }else{
      return(
        <span>Not Available</span>
      )
    }
  }
  render() {
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() =>this.toggle('1')}
            >
              Enteries
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() =>this.toggle('2')}
            >
              Assets
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col md="12" className="table-container">
                {this.renderEntriesTable()}
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
            <Col md="12" className="table-container">
              {this.renderAssetsTable()}
            </Col>
            </Row>
          </TabPane>
        </TabContent>
      </div>
    );
  }
}
