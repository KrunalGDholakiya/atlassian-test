import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import createHistory from 'history/createBrowserHistory';
import 'bootstrap/dist/css/bootstrap.min.css';
import Root from './containers/Root';
const history = createHistory();

render(
    <AppContainer>
        <Root history={history} />
    </AppContainer>,
    document.getElementById('root')
);

if (module.hot) {
    module.hot.accept('./containers/Root', () => {
        const newHistory = createHistory();
        const NewRoot = require('./containers/Root').default;
        render(
            <AppContainer>
                <NewRoot history={newHistory} />
            </AppContainer>,
            document.getElementById('root')
        );
    });
}
