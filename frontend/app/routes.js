import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Landing from './containers/landing/Landing';
import SpaceExplorer from './containers/spaceexplorer/SpaceExplorer';
import ErrorPage from './containers/errorpage/ErrorPage';
export default (
	<Switch>
		<Route exact path="/" component={Landing} />
		<Route path="/spaceexplorer/:id" component={SpaceExplorer} />
		<Route path="/error" component={ErrorPage} />
	</Switch>
);
